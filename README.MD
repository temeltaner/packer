# Package Challenge Lib

This is the official supported Java library for the Package Selector API.
https://www.mobiquityinc.com/

## Getting Started

These instructions will get you a copy of the packer up and running on your local machine for development and testing purposes.

### Prerequisites

 - Java 1.8

 - Maven


### Build
####Manual installation
Open a command console into the packer folder and run the following. 
```
mvn clean package
```

This will run all the unit tests then compile the class file and build the jar file under /target directory
```
target/packer-<version>.jar
```

If you would like a copy of the javadocs, use

```
mvn javadoc:javadoc
```


### Running the tests

Unit tests are under /src/test/java/com/mobiquityinc/
they can be run with 
```
mvn test
``` 

Maven Installation
------------------

Add the following to your pom.xml

```xml
    <dependency>
        <groupId>com.mobiquityinc</groupId>
        <artifactId>packer</artifactId>
        <version>VERSION</version>
        <!-- Replace VERSION with the version you want to use -->
    </dependency>
```