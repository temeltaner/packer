package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.util.impl.FileUtilImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Project packer
 * Created by taner on 02.03.2019
 */
public class PackerTest {
    @Before
    public void setUp() {

    }

    @Test
    public void parseFileContentItem_ShouldReturnContentItemsTest() {
        String file = FileUtilImpl.getInstance().filePath("src", "test", "resources", "FileTest.txt");
        String result = Packer.pack(file);
        Assert.assertTrue(result.equals("1,6\n" +
                "-\n" +
                "2,7\n" +
                "8,9"));
    }

    @Test
    public void exception_Test() {
        APIException exception = new APIException("TEST","TEST");
        Assert.assertTrue(exception.getExceptionCode().equals("TEST"));
    }

    @Test(expected = APIException.class)
    public void loadFile_ShouldReturnIOExceptionTest() {
        String file = "FileTest.txt";
        Packer.pack(file);
    }


    /*

    Method converted to private_modifier (Refactor)

    @Test
    public void prepareOutput_givenListOutputTest() {
        ArrayList<ArrayList<Integer>> nodes = new ArrayList<>();
        ArrayList<Integer> integers1 = new ArrayList<Integer>(Arrays.asList(5, 7, 8));
        ArrayList<Integer> integers2 = new ArrayList<Integer>(Arrays.asList(1, 2));
        nodes.add(integers1);
        nodes.add(integers2);

        String expected = "5,7,8" + Constants.LINE_SEPERATOR + "1,2";
        Assert.assertEquals(expected, Packer.prepareOutput(nodes));
    }

    */
}
