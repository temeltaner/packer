package com.mobiquityinc.packer.util;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.model.Content;
import com.mobiquityinc.packer.util.impl.FileUtilImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;

/**
 * Project packer
 * Created by taner on 02.03.2019
 */
public class FileUtilTest {

    @Before
    public void setUp() {

    }

    @Test
    public void parseFileContentItem_WithNullValuesTest() {
        List<Content> packages = FileUtilImpl.getInstance().loadFile(null);
        Assert.assertThat(null, is(packages));
    }

    @Test
    public void loadFile_ShouldReturnValuesTest() {
        String file = FileUtilImpl.getInstance().filePath("src", "test", "resources", "FileTest.txt");
        List<Content> packages = FileUtilImpl.getInstance().loadFile(file);
        Assert.assertTrue(packages.size() > 0);
    }

    @Test(expected = APIException.class)
    public void loadFile_ShouldReturnIOExceptionTest() {
        String file = "FileTest.txt";
        FileUtilImpl.getInstance().loadFile(file);
    }

    @Test(expected = APIException.class)
    public void loadFile_whenEmptyFile_InapropriateFileContentTest() {
        String file = FileUtilImpl.getInstance().filePath("src", "test", "resources", "FileTest2.txt");
        FileUtilImpl.getInstance().loadFile(file);
    }
}
