package com.mobiquityinc.packer.parser;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.model.Content;
import com.mobiquityinc.packer.model.ContentModel;
import com.mobiquityinc.packer.parser.impl.ParserImpl;
import com.mobiquityinc.packer.util.impl.FileUtilImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;

/**
 * Project packer
 * Created by taner on 02.03.2019
 */
public class ParserTest {

    List<Content> fileContentMap;

    @Before
    public void setUp() {
        String file = FileUtilImpl.getInstance().filePath("src", "test", "resources", "FileTest.txt");
        fileContentMap = FileUtilImpl.getInstance().loadFile(file);
    }

    @Test
    public void parseFileContentItem_WithNullValuesTest() {
        List<ContentModel> contentModels = ParserImpl.getInstance().parseFileContentItem(null);
        Assert.assertThat(null, is(contentModels));
    }

    @Test
    public void parseFileContentItem_ShouldReturnContentItemsTest() {
        List<ContentModel> contentModels = ParserImpl.getInstance().parseFileContentItem(fileContentMap);
        Assert.assertTrue(contentModels.size() > 0);
    }

    @Test(expected = APIException.class)
    public void parseFileContentItem_ShouldThrowAPIException_MaxWeightTest() {
        ParserImpl.getInstance().parseFileContentItem(Arrays.asList(new Content("120", "(2,33.80,€40)")));
    }

    @Test(expected = APIException.class)
    public void parseFileContentItem_ShouldThrowAPIException_WeightTest() {
        ParserImpl.getInstance().parseFileContentItem(Arrays.asList(new Content("12", "(2,100.80,€40)")));
    }

    @Test(expected = APIException.class)
    public void parseFileContentItem_ShouldThrowAPIException_MaxPackageCountTest() {
        ParserImpl.getInstance().parseFileContentItem(Arrays.asList(new Content("99", "(1,90.72,€13) (2,33.80,€40) (3,43.15,€10) (4,37.97,€16) (5,46.81,€36) (6,48.77,€79) (7,81.80,€45) (8,19.36,€79) (9,6.76,€64) (1,85.31,€29) (2,14.55,€74) (3,3.98,€16) (4,26.24,€55) (5,63.69,€52) (6,76.25,€75) (7,60.02,€74)")));
    }
}
