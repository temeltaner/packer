package com.mobiquityinc.packer.mode;

import com.mobiquityinc.packer.model.Content;
import com.mobiquityinc.packer.model.ContentItem;
import com.mobiquityinc.packer.model.ContentModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

/**
 * Project packer
 * Created by taner on 02.03.2019
 */
public class ModelTest {
    Content content;
    String contentValue = "(1,45.3,€56)";
    ContentModel contentModel;

    @Before
    public void setUp() {
        content = new Content("34", "(1,45.3,€56)");
        contentModel = new ContentModel(1, Arrays.asList(new ContentItem(1, 1, 1, '€')));
    }

    @Test
    public void contentTest() {
        Assert.assertTrue(content.getMaxWeight() == 34d);
        Assert.assertTrue(content.getValue().equals(contentValue));
    }

    @Test
    public void contentItemTest() {
        ContentItem contentItem = new ContentItem(1, 1, 1, '€');
        Assert.assertTrue(contentItem.getCost() == 1);
        Assert.assertTrue(contentItem.getIndex() == 1);
        Assert.assertTrue(contentItem.getWeight() == 1);
        Assert.assertTrue(contentItem.getCostSymbol() == '€');
    }

    @Test
    public void contentItem_EqualityTest() {
        ContentItem contentItem = new ContentItem(1, 1, 1, '€');
        Assert.assertTrue(contentItem.equals(new ContentItem(1, 1, 1, '€')));
    }

    @Test
    public void contentModelTest() {
        Assert.assertTrue(contentModel.getMaxWeight() == 1);
        Assert.assertTrue(contentModel.getContentItem().size() == 1);
    }
}

