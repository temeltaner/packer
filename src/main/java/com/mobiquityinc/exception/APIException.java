package com.mobiquityinc.exception;

/**
 * <h3> Library Base Exception Type </h3>
 *
 *
 * Project packer
 * Created by taner on 02.03.2019
 */
public class APIException extends RuntimeException {

    private final String exceptionCode;

    /**
     * Since it is a small project, more than one exception type is not used.
     * Exception types separated by Code.
     * Empty constructor is not available. Exceptions should initiate with Code.
     *
     * @param exceptionCode Exception Code for Exception Types
     *
     *   Exception Codes
     *   UNKNOWN ERROR-> PCK-001
     *   FILE NOT FOUND ERROR -> PCK-002
     *   FILE CONTENT NOT APPROPRIATE ERROR -> PCK-003
     *   WEIGHT SHOULD LESS THAN 100 ERROR -> PCK-004
     *   MAX PACKAGE COUNT ERROR -> PCK-005
     *   MAX ITEM WEIGHT ERROR -> PCK-006
     *   MAX ITEM COST  -> PCK-007
     *    see {@link com.mobiquityinc.packer.util.Constants}
     * @param message Message details for exception
     */
    public APIException(String exceptionCode, String message) {
        super(message);
        this.exceptionCode = exceptionCode;
    }

    public String getExceptionCode() {
        return exceptionCode;
    }
}
