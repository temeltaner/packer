package com.mobiquityinc.packer.util.impl;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.model.Content;
import com.mobiquityinc.packer.util.Constants;
import com.mobiquityinc.packer.util.IFileUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Project packer
 * Created by taner on 02.03.2019
 */
public class FileUtilImpl implements IFileUtil {

    private static Logger LOGGER = LoggerFactory.getLogger(FileUtilImpl.class);

    private static FileUtilImpl instance;

    private FileUtilImpl() {
    }

    /**
     * Instance initialization
     *
     * @return instance of FileUtilImpl
     */
    public static FileUtilImpl getInstance() {
        if (instance == null) {
            synchronized (FileUtilImpl.class) {
                if (instance == null) {
                    LOGGER.debug("FileUtilImpl Instance created");
                    instance = new FileUtilImpl();
                }
            }
        }
        return instance;
    }

    /**
     * Java 8 has added a new method called lines() in Files class which can be used to read a file line by line in Java.
     * The beauty of this method is that it reads all lines from a file as Stream of String,
     * which is populated lazily as the stream is consumed.
     *
     * @param filePath File to read
     * @return File Content converts Models
     * @throws APIException Module Base Exception
     */
    public List<Content> loadFile(String filePath) throws APIException {
        if (filePath == null) {
            throw new APIException(Constants.PCK_FILENOTFOUND_EXCODE, "File Not Found");
        }

        LOGGER.debug("Resource : " + filePath + " processing..");

        List<Content> resourceList = new ArrayList<>();
        try (Stream<String> lines = Files.lines(Paths.get(filePath))) {
            lines.filter(line -> line.contains(PACKAGE_MAX_WEIGHT_DELIMITER)).forEach(
                    line -> {
                        String lineArray[] = line.split(PACKAGE_MAX_WEIGHT_DELIMITER);
                        Content content = new Content(lineArray[0].trim(), lineArray[1].trim());
                        LOGGER.debug("Content Parsed :" + content.toString());
                        resourceList.add(content);
                    }
            );
        } catch (IOException e) {
            throw new APIException(Constants.PCK_FILENOTFOUND_EXCODE, "File Not Found");
        }

        LOGGER.debug("Resource : " + filePath + " loaded package size : " + resourceList.size());

        if (resourceList.size() == 0)
            throw new APIException(Constants.PCK_FILE_CONTENT_NOT_APPR_EXCODE, "File content not appropriate");

        return resourceList;
    }

    /**
     * Path seperator utility method for different OperatingSystems
     * Instead of this apache-common-lib can be used
     *
     * @param parts Parts of file paths
     * @return combined path with FileSeperator
     */
    public String filePath(String... parts) {
        return Arrays.stream(parts).map(String::valueOf).collect(Collectors.joining(File.separator));
    }

}
