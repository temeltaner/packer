package com.mobiquityinc.packer.util;

/**
 * <h3> Module Constants </h3>
 * Project packer
 * Created by taner on 02.03.2019
 */
public interface Constants {

    /**
     * We can create custom exceptions for each exception-code !
     */
    String PCK_UNKNOWN_EXCODE = "PCK-001";
    String PCK_FILENOTFOUND_EXCODE = "PCK-002";
    String PCK_FILE_CONTENT_NOT_APPR_EXCODE = "PCK-003";
    String PCK_WEIGHT_SHOULD_LESS100_EXCODE = "PCK-004";
    String PCK_MAX_PACKAGE_COUNT_EXCODE = "PCK-005";
    String PCK_MAX_ITEM_WEIGHT_EXCODE = "PCK-006";
    String PCK_MAX_ITEM_COST_EXCODE = "PCK-007";

    String LINE_SEPERATOR = System.getProperty("line.separator");
}
