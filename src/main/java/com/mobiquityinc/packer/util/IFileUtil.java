package com.mobiquityinc.packer.util;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.model.Content;

import java.util.List;

/**
 * Project packer
 * Created by taner on 02.03.2019
 */
public interface IFileUtil {

    /*
        Packages and MaxWeight delimiter
     */
    String PACKAGE_MAX_WEIGHT_DELIMITER = ":";

    /**
     * This method will read lines in the file which are two strings separated by delimiter (:)
     * Contents will insert into a List as key-value model
     *
     * @param filePath File to read
     * @return List of packages
     * @throws APIException Module Base Exception
     */
    List<Content> loadFile(String filePath) throws APIException;
}
