package com.mobiquityinc.packer.parser.impl;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.model.Content;
import com.mobiquityinc.packer.model.ContentItem;
import com.mobiquityinc.packer.model.ContentModel;
import com.mobiquityinc.packer.parser.IParser;
import com.mobiquityinc.packer.util.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * <h3>Parser Implementation</h3>
 * <p>
 * Project packer
 * Created by taner on 02.03.2019
 */
public class ParserImpl implements IParser {

    private static Logger LOGGER = LoggerFactory.getLogger(ParserImpl.class);
    private static ParserImpl instance;

    /**
     * Instance generation disabled for external
     */
    private ParserImpl() {
    }

    /**
     * Instance initialization
     *
     * @return instance of ParserImpl
     */
    public static ParserImpl getInstance() {
        if (instance == null) {
            synchronized (ParserImpl.class) {
                if (instance == null) {
                    LOGGER.debug("ParserImpl Instance created");
                    instance = new ParserImpl();
                }
            }
        }
        return instance;
    }

    @Override
    public List<ContentModel> parseFileContentItem(List<Content> contentItems) {
        LOGGER.debug("File Content Items processing..");
        if (contentItems == null)
            return null;

        List<ContentModel> contentModels = new ArrayList<>();
        for (Content entry : contentItems) {
            String[] stringItems = entry.getValue().split(" ");
            ContentModel model = populateContentModel(entry.getMaxWeight(), stringItems);
            LOGGER.debug("Generated Model :" + model.toString());
            contentModels.add(model);
        }
        return contentModels;
    }

    /**
     * @param maxWeight
     * @param stringItems
     * @return
     */
    private ContentModel populateContentModel(double maxWeight, String[] stringItems) {
        LOGGER.debug("Content Models processing..");
        List<ContentItem> items = new ArrayList<>();
        for (String stringItem : stringItems) {
            if (items.size() == MAX_PACKAGE_COUNT) {
                throw new APIException(Constants.PCK_MAX_PACKAGE_COUNT_EXCODE, "The package count sould be less then 15");
            }

            String[] itemDetails = stringItem.split(",");
            Integer id = Integer.parseInt(itemDetails[0].substring(1));
            double weight = Double.parseDouble(itemDetails[1]);
            if (weight > MAX_WEIGHT) {
                throw new APIException(Constants.PCK_MAX_ITEM_WEIGHT_EXCODE, "The Item Weight sould be less or equal 100");
            }

            double cost = Double.parseDouble(itemDetails[2].substring(1, itemDetails[2].length() - 1));
            char costSymbol = itemDetails[2].charAt(0);
            if (weight < maxWeight) {
                ContentItem item = new ContentItem(id, weight, cost, costSymbol);
                LOGGER.debug("Parsed : " + item.toString());
                items.add(item);
            }
        }
        return new ContentModel(maxWeight, items);
    }
}
