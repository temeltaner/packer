package com.mobiquityinc.packer.parser;

import com.mobiquityinc.packer.model.Content;
import com.mobiquityinc.packer.model.ContentModel;

import java.util.List;

/**
 * Project packer
 * Created by taner on 02.03.2019
 */
public interface IParser {

    int MAX_WEIGHT = 100;
    int MAX_PACKAGE_COUNT = 15;

    /**
     * This method parsing the given file and return file content java object model
     *
     * @param contentItems Content Items
     * @return Parsed File Content Model
     */
    List<ContentModel> parseFileContentItem(List<Content> contentItems);

}
