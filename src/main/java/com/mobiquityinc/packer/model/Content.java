package com.mobiquityinc.packer.model;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.util.Constants;

/**
 * <h3>Item First Level ParsingModel</h3>
 *
 * Project packer
 * Created by taner on 02.03.2019
 */
public class Content {

    /**
     * Conent constructor contains some validations and throws APIException
     * - Weight of Package should be less or equal 100
     *
     * @param maxWeight MaxWeight of Package
     * @param value String value of packages
     */
    public Content(String maxWeight, String value) {
        double dMaxWeight = Double.valueOf(maxWeight);
        if (dMaxWeight > 100)
            throw new APIException(Constants.PCK_WEIGHT_SHOULD_LESS100_EXCODE, "Weight of Package should be less or equal 100");

        this.maxWeight = Double.valueOf(maxWeight);
        this.value = value;
    }

    private final double maxWeight;
    private final String value;

    public double getMaxWeight() {
        return maxWeight;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Content{" +
                "maxWeight=" + maxWeight +
                ", value='" + value + '\'' +
                '}';
    }
}
