package com.mobiquityinc.packer.model;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.util.Constants;

import java.util.Objects;

/**
 * <h3> Package Model </h3>
 * <p>
 * Project packer
 * Created by taner on 02.03.2019
 */
public class ContentItem {

    /**
     * Contsructor checks cost and weight validations and throws APIException
     * - The Item Cost sould be less or equal 100
     * - The Item Weight sould be less or equal 100
     *
     * @param index index of package
     * @param weight weight of package
     * @param cost cost of package
     * @param costSymbol money symbol of package
     */
    public ContentItem(Integer index, double weight, double cost, char costSymbol) {

        if (cost > 100) {
            throw new APIException(Constants.PCK_MAX_ITEM_COST_EXCODE, "The Item Cost sould be less or equal 100");
        }
        if (weight > 100) {
            throw new APIException(Constants.PCK_MAX_ITEM_WEIGHT_EXCODE, "The Item Weight sould be less or equal 100");
        }

        this.index = index;
        this.weight = weight;
        this.cost = cost;
        this.costSymbol = costSymbol;
    }

    private final Integer index;
    private final double weight;
    private final double cost;
    private final char costSymbol;

    public Integer getIndex() {
        return index;
    }

    public double getWeight() {
        return weight;
    }

    public double getCost() {
        return cost;
    }

    public char getCostSymbol() {
        return costSymbol;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContentItem that = (ContentItem) o;
        return index.equals(that.index);
    }

    @Override
    public int hashCode() {
        return Objects.hash(index);
    }

    @Override
    public String toString() {
        return "ContentItem{" +
                "index=" + index +
                ", weight=" + weight +
                ", cost=" + cost +
                ", costSymbol=" + costSymbol +
                '}';
    }
}