package com.mobiquityinc.packer.model;

import java.util.List;

/**
 * <h3> Package Group Model </h3>
 *
 * Project packer
 * Created by taner on 02.03.2019
 */
public class ContentModel {

    /**
     * @param maxWeight Max weight of package
     * @param contentItem Content Item
     */
    public ContentModel(double maxWeight, List<ContentItem> contentItem) {
        this.maxWeight = maxWeight;
        this.contentItem = contentItem;
    }

    private final double maxWeight;
    private final List<ContentItem> contentItem;

    public double getMaxWeight() {
        return maxWeight;
    }

    public List<ContentItem> getContentItem() {
        return contentItem;
    }
}
