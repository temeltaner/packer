package com.mobiquityinc.packer.combination;

import com.mobiquityinc.packer.model.ContentModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Project packer
 * Created by taner on 02.03.2019
 */
public interface IBestPackageSelector {

    /**
     *  Content model calculation for best package selection
     *
     * @param contentnModels ContentModel List
     * @return BestPackageList for each Packages
     */
    ArrayList<ArrayList<Integer>> contentGeneration(List<ContentModel> contentnModels);
}
