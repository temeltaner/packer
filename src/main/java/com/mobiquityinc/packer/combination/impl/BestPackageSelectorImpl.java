package com.mobiquityinc.packer.combination.impl;

import com.mobiquityinc.packer.combination.IBestPackageSelector;
import com.mobiquityinc.packer.model.ContentItem;
import com.mobiquityinc.packer.model.ContentModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * <h3> Best Package Selector Implementation </h3>
 *
 * Project packer
 * Created by taner on 02.03.2019
 */
public class BestPackageSelectorImpl implements IBestPackageSelector {
    private static Logger LOGGER = LoggerFactory.getLogger(BestPackageSelectorImpl.class);

    private static BestPackageSelectorImpl instance;

    private BestPackageSelectorImpl() {
    }

    /**
     * Instance initialization
     * @return instance of BestPackageSelectorImpl
     */
    public static BestPackageSelectorImpl getInstance() {
        if (instance == null) {
            synchronized (BestPackageSelectorImpl.class) {
                if (instance == null) {
                    LOGGER.debug("BestPackageSelectorImpl Instance created");
                    instance = new BestPackageSelectorImpl();
                }
            }
        }
        return instance;
    }


    @Override
    public ArrayList<ArrayList<Integer>> contentGeneration(List<ContentModel> contentnModels) {
        LOGGER.debug("File Content Model Items Best Package selection content generation started..");
        ArrayList<ArrayList<Integer>> bestPackageIds = new ArrayList<>();
        for (ContentModel contentModel : contentnModels) {
            ArrayList<ArrayList<ContentItem>> items = generateAllCombinations(contentModel);
            ArrayList<ContentItem> el = getBestPackage(items, contentModel.getMaxWeight());
            if (el.size() > 0) {
                ArrayList<Integer> list = new ArrayList<>();
                for (ContentItem contentItem : el) {
                    list.add(contentItem.getIndex());
                }
                bestPackageIds.add(list);
            } else {
                bestPackageIds.add(new ArrayList<Integer>());
            }
        }
        LOGGER.debug("File Content Model Items Best Package selection content generation completed..");
        return bestPackageIds;
    }

    /**
     * Best package selection from all combinations
     * @param combinedElements
     * @param maxWeight
     * @return Best Packages from all combinations
     */
    private ArrayList<ContentItem> getBestPackage(ArrayList<ArrayList<ContentItem>> combinedElements, double maxWeight) {
        LOGGER.debug("Best Package processing for Package maxWeight : " + maxWeight);
        ArrayList<ContentItem> bestComb = new ArrayList<ContentItem>();
        double bestCost = 0;
        for (ArrayList<ContentItem> element : combinedElements) {
            double bestWeight = maxWeight;
            double totalWeight = calculateTotalWeight(element);
            if (totalWeight > maxWeight) {
                continue;
            } else {
                double totalCost = calculateTotalCost(element);
                if (totalCost > bestCost) {
                    bestCost = totalCost;
                    bestComb = element;
                    bestWeight = totalWeight;
                } else if (totalCost == bestCost) {
                    if (totalWeight < bestWeight) {
                        bestCost = totalCost;
                        bestComb = element;
                        bestWeight = totalWeight;
                    }
                }
            }
        }
        LOGGER.debug("Best Package generation completed.. ");
        return bestComb;
    }

    /**
     *  Generation of all Package Combination
     * @param contentModel
     * @return
     */
    private ArrayList<ArrayList<ContentItem>> generateAllCombinations(ContentModel contentModel) {
        LOGGER.debug("File Content Model Item Combination generating..");
        ArrayList<ArrayList<ContentItem>> combinedElements = new ArrayList<>();
        for (int i = 0; i < contentModel.getContentItem().size(); i++) {
            ContentItem currentItem = contentModel.getContentItem().get(i);
            int combinationSize = combinedElements.size();
            for (int j = 0; j < combinationSize; j++) {
                ArrayList<ContentItem> combination = combinedElements.get(j);
                ArrayList<ContentItem> newCombination = new ArrayList<>(combination);
                newCombination.add(currentItem);
                combinedElements.add(newCombination);
            }
            ArrayList<ContentItem> current = new ArrayList<ContentItem>();
            current.add(currentItem);
            combinedElements.add(current);
        }
        LOGGER.debug("File Content Model Item Combination generated..");
        return combinedElements;
    }

    /**
     *  Total Cost of PackageItems
     * @param items
     * @return
     */
    private double calculateTotalCost(List<ContentItem> items) {
        LOGGER.debug("Total Cost processing for package..");
        double totalCost = 0;
        for (ContentItem i : items) {
            totalCost += i.getCost();
        }
        return totalCost;
    }

    /**
     *  Total Weight of PackageItems
     * @param items
     * @return
     */
    private double calculateTotalWeight(List<ContentItem> items) {
        LOGGER.debug("Total Weight processing for package..");
        double totalWeight = 0;
        for (ContentItem i : items) {
            totalWeight += i.getWeight();
        }
        return totalWeight;
    }
}
