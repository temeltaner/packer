package com.mobiquityinc.packer;

import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.packer.combination.impl.BestPackageSelectorImpl;
import com.mobiquityinc.packer.model.Content;
import com.mobiquityinc.packer.model.ContentModel;
import com.mobiquityinc.packer.parser.impl.ParserImpl;
import com.mobiquityinc.packer.util.Constants;
import com.mobiquityinc.packer.util.impl.FileUtilImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Project packer
 * Created by taner on 02.03.2019
 */
public class Packer {

    private static Logger LOGGER = LoggerFactory.getLogger(Packer.class);

    /**
     * Library developed with ThreadSafe Singleton design pattern , This method shouldn't be static ,
     * this class should also be developed as singleton
     *
     * @param filePath path of file that will be read
     *                 E.g. File Content -> 81 : (1,33.38,€45) (2,88.62,€98) (3,78.48,€3) (4,72.30,€76) (5,30.18,€9) (6,46.34,€48)
     *
     * @return item index numbers	are	separated by comma.	E.g.
     *       4
     *       -
     *       2,7
     *       8,9
     *
     * @throws APIException Library Exception base, Codes are below
     *   UNKNOWN ERROR-> PCK-001
     *   FILE NOT FOUND ERROR -> PCK-002
     *   FILE CONTENT NOT APPROPRIATE ERROR -> PCK-003
     *   WEIGHT SHOULD LESS THAN 100 ERROR -> PCK-004
     *   MAX PACKAGE COUNT ERROR -> PCK-005
     *   MAX ITEM WEIGHT ERROR -> PCK-006
     *   MAX ITEM COST  -> PCK-007
     *   see {@link com.mobiquityinc.packer.util.Constants}
     */
    public static String pack(String filePath) throws APIException {
        LOGGER.debug("FilePath : " + filePath + " processing..");
        try {
            List<Content> contentList = FileUtilImpl.getInstance().loadFile(filePath);
            List<ContentModel> contentModels = ParserImpl.getInstance().parseFileContentItem(contentList);
            return prepareOutput(BestPackageSelectorImpl.getInstance().contentGeneration(contentModels));
        } catch (Exception e) {
            if (e instanceof APIException)
                throw new APIException(((APIException) e).getExceptionCode(), e.getMessage());

            LOGGER.error(e.getMessage(), e);
            throw new APIException(Constants.PCK_UNKNOWN_EXCODE, e.getMessage());
        }
    }


    /**
     * Styled Output generator
     *
     * @param bestPackageIds Selected Package IDs
     * @return Output
     */
    private static String prepareOutput(ArrayList<ArrayList<Integer>> bestPackageIds) {
        LOGGER.debug("Package Items Outputs processing..");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bestPackageIds.size(); i++) {
            ArrayList<Integer> packages = bestPackageIds.get(i);
            if (packages.size() > 0) {
                sb.append(packages.stream().map(String::valueOf).collect(Collectors.joining(",")));
                if (bestPackageIds.size() > (i + 1))
                    sb.append(Constants.LINE_SEPERATOR);
            } else {
                sb.append("-" + Constants.LINE_SEPERATOR);
            }
        }
        LOGGER.debug("Generated Outputs : " + sb.toString());
        return sb.toString();
    }

}
